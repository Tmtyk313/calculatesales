package jp.alhinc.tomita_yuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {
	public static void main(String[] args) {
											// コマンドライン引数が0or1以上
		if(args.length != 1){
	    	System.out.println("予期せぬエラーが発生しました");
	    	return;
	    }

											// 変数宣言2
		HashMap<String, String> branch = new HashMap<String,String>();
		HashMap<String, Long> branchSales = new HashMap<String,Long>();
		BufferedReader br = null;
		long longNumber =9999999999L;
		Long salesListkey;
		String branchName = "支店";
		if(!fileIutput(args[0],"branch.lst",branch,branchSales,"[0-9]{3}","支店")) {
			return;
		}


											//売上ファイル抽出//
		File dir = new File(args[0]);
		String[] branchInfo = dir.list();

											// ArrayListにて保管するための処理
		ArrayList<String> branchList = new ArrayList<String>();
		for(int i = 0; i < branchInfo.length; i++ ) {

											// 条件にマッチするものを選択
			File branchInfofile = new File(args[0],branchInfo[i]);
			if(branchInfo[i].matches("[0-9]{8}.rcd") && branchInfofile.isFile()) {
				branchList.add(branchInfo[i]);
//				System.out.println(branchList.get(i));
			}
		}

											// 連番処理
		for(int i = 0; i < branchList.size() - 1; i++  ) {
			String numberOnly = branchList.get(i).substring(0, 8);
			String numberOnlyplus = branchList.get(i + 1).substring(0, 8);
			int digit = Integer.parseInt(numberOnly);
			int digitPlus = Integer.parseInt(numberOnlyplus);

//			System.out.println(digit);
//			System.out.println(digitPlus - digit);

			if(digitPlus - digit != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

											// 売上ファイルごとの読込//

		for(int i = 0; i < branchList.size(); i++ ) {

			ArrayList<String> salesList = new ArrayList<String>();

			try {
				File file = new File(args[0],branchList.get(i));
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String s;

											// 売上ファイルを１行ごと読込
				while((s = br.readLine()) != null){
					salesList.add(s);
				}
				if(salesList.size() <= 1 || salesList.size() >= 3 ) {
					System.out.println( branchInfo[i] + "のフォーマットが不正です");
					return;
				}

											// 売上の支店コードを使ってマップ内の売上を出力//
											// 支店に該当がなかった場合のエラー処理//
				if(branchSales.containsKey(salesList.get(0))) {
					salesListkey = branchSales.get(salesList.get(0));
				} else {
					System.out.println(  branchInfo[i] + "の" + branchName + "コードが不正です");
					return;
				}

											// 予期せぬエラーが発生した場合の処理
				if(!salesList.get(1).matches("[0-9]+")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

											// (salesList.get(1))をparselongにキャスト
				long salesValues = Long.parseLong(salesList.get(1));

											// 10桁を超えた場合のエラー//
				if(salesListkey + salesValues > longNumber){
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.put((salesList.get(0)) ,(salesListkey + salesValues)) ;

			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if(!fileOutput(args[0],"branch.out",branch,branchSales)) {
			return;
		}
	}

											// 集計結果出力

	public static boolean fileOutput(String fileFolder, String fileName, HashMap<String, String> codeName, HashMap<String,Long> totalSales ) {

		BufferedWriter bw = null;

		try{
			File file = new File(fileFolder, fileName);
			FileWriter fw = new FileWriter(file);
			 bw = new BufferedWriter(fw);
			for(String str : codeName.keySet())  {
//				System.out.println(str + "," + codeName.get(str) + "," + totalSales.get(str));
				bw.write(str + "," + codeName.get(str) + "," + totalSales.get(str) );
				bw.newLine();
			} return true;
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	}
												//支店定義ファイル読込//
	public static boolean fileIutput(String fileFolder, String fileName,HashMap<String, String> codeName, HashMap<String,Long> totalSales,String conditions,String branchName) {

		BufferedReader br = null;
		long zeroValue = 0;

		try {
		    File file = new File(fileFolder,fileName);

		    								// 支店定義ファイルが存在しない場合のエラー処理
		    if(!file.exists()) {
		    	System.out.println(branchName + "定義ファイルが存在しません");
		    	return false;
		    }
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

											// データを1行ごとに読み込むための処理
			while((line = br.readLine()) != null){

											// keyとvalueに分けるための処理
				String[] code = line.split(",");

											// 支店定義ファイルのフォーマットの精査
				if(code.length == 2 && code[0].matches(conditions)) {

											// mapへputする処理
				codeName.put(code[0], code[1]);
				totalSales.put(code[0], zeroValue);
				} else {
					System.out.println(branchName + "定義ファイルのフォーマットが不正です");
					return false;
				}
			}
			return true;

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	}
}